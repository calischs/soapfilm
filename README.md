## Soap film wind tunnel


<img src='img/thread-free-small.mp4'>


Reservoir of soapy water at the top, needle valve to control flow, monofilament line forms the rails.  Solution is collected at the bottom and pumped back to the top.

<img src='img/overall.jpg' height=450px>
<img src='img/tank.jpg' height=450px>


I first tried single color power LEDs to see interference fringes.  I didn't observe very clear fringes.  Then I switched to a low-pressure sodium lamp.  I saw very clear fringes.

<img src='img/lps-warmup.jpg' height=220px>
<img src='img/first-fringes.jpg' height=220px>


### First videos

line

<img src='vid/line-small.mp4'>

finger

<img src='vid/finger-small.mp4'>

I need to fix the flicker from the ballast for my low pressure sodium lamp.

### Experiments

<img src='img/wave-generator.jpg' height=450px>

[thread free oscillation](https://youtu.be/ZzPdvUJ6nhM)

[thread driven oscillation](https://www.youtube.com/watch?v=Gmo8fJyLdgg)

[driven protoflapping membrane](https://www.youtube.com/watch?v=wPpQXo0StCk)





